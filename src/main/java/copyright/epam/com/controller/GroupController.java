package copyright.epam.com.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import copyright.epam.com.dto.GroupDTO;
import copyright.epam.com.entity.Group;
import copyright.epam.com.model.GroupModel;
import copyright.epam.com.service.GroupService;

@RestController
public class GroupController {
	
	@Autowired
	GroupService groupService;
	
	@RequestMapping(value = "/api/group", method = RequestMethod.GET)
	public ResponseEntity<List<GroupModel>> getGroups() {		
		List<Group> groups = groupService.findAll();
		List<GroupModel> groupsModel = new ArrayList<>(groups.size());		
		for (Group group : groups) {
			groupsModel.add(new GroupModel(group));
		}				
		return new ResponseEntity<>(groupsModel, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/group/{groupId}", method = RequestMethod.GET)
	public ResponseEntity<GroupModel> getGroupById(@PathVariable Long groupId) {		
		Group group = groupService.findById(groupId);		
		GroupModel groupModel = new GroupModel(group);			
		return new ResponseEntity<>(groupModel, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/api/group/{groupId}", method = RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteGroup(@PathVariable Long groupId) {
		groupService.delete(groupId);		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/group", method = RequestMethod.POST)
	public ResponseEntity<GroupModel> addGroup(@RequestBody GroupDTO grouDTO){			
		Group group = new Group(grouDTO);
		groupService.create(group);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/api/group/{groupId}", method = RequestMethod.PUT)
	public ResponseEntity<GroupModel> updateGroup(@PathVariable Long groupId, @RequestBody GroupDTO updateGroupDTO){	
		Group group = groupService.findById(groupId);
		group.setName(updateGroupDTO.getName());
		group.setDescription(updateGroupDTO.getDescription());
		groupService.update(group);	
		GroupModel groupModel = new GroupModel(group);
		return new ResponseEntity<>(groupModel , HttpStatus.CREATED);
	}
}
