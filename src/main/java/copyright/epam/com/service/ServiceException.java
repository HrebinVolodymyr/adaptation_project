package copyright.epam.com.service;

public class ServiceException extends RuntimeException{
	
	private static final long serialVersionUID = -6722290583260717251L;

	public ServiceException(String message, Throwable cause) {
		super(message, cause);		
	}

	public ServiceException(String message) {
		super(message);		
	}
	
	

}
