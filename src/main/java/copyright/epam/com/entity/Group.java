package copyright.epam.com.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import copyright.epam.com.dto.GroupDTO;
import java.util.Objects;

@Entity
@Table(name = "user_group")
public class Group {
	
	public Group() {
		super();
	}
	
	public Group(GroupDTO groupDTO) {
		super();
		this.name = groupDTO.getName();
		this.description = groupDTO.getDescription();
	}
		
	public Group(Long id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name", nullable = false, unique = true, length = 30)
	private String name;
	
	@Column(name = "description", nullable = true, length = 200)
	private String description;
	
	@OneToMany(mappedBy = "userGroup", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<User> users;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, description);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Group) {
			Group that = (Group) object;
			return Objects.equals(this.id, that.id) && Objects.equals(this.name, that.name)
					&& Objects.equals(this.description, that.description);
		}
		return false;
	}

}
