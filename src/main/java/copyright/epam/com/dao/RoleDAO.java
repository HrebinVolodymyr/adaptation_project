package copyright.epam.com.dao;

import copyright.epam.com.entity.Role;

public interface RoleDAO extends GeneralDAO<Role, Long>{

}
