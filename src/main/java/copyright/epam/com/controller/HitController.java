package copyright.epam.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import copyright.epam.com.entity.Hit;
import copyright.epam.com.service.HitService;

@RestController
public class HitController {
	
	@Autowired
	HitService hitService;
	
	@RequestMapping(value = "/api/hit", method = RequestMethod.GET)
	public ResponseEntity<List<Hit>> getHits() {		
		List<Hit> hits = hitService.findAll();				
		return new ResponseEntity<>(hits, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/hit", method = RequestMethod.POST)
	public ResponseEntity<Hit> addGroup(@RequestBody Hit hit){			
		hitService.create(hit);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
