package copyright.epam.com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import copyright.epam.com.dao.GroupDAO;
import copyright.epam.com.entity.Group;

@Component
public class GroupDaoImpl  implements GroupDAO{
	
	@PersistenceContext
	EntityManager em;

	@Override
	public List<Group> findAll() {
		return em.createQuery("SELECT g FROM Group g", Group.class)
		        .getResultList();
	}

	@Override
	public Group findById(Long id) {		
		return em.find(Group.class, id);
	}

	@Override
	public void create(Group entity) {
		em.persist(entity);
		
	}

	@Override
	public void update(Group entity) {
		em.merge(entity);		
	}
	
	@Override
	public void delete(Long id) {
		em.remove(em.getReference(Group.class, id));
	}
}
