package copyright.epam.com.dao;

import java.util.List;

public interface GeneralDAO<T, I> {
	
	List<T> findAll();

	T findById(I id);

	void create(T entity);

	void update(T entity);	
	
	void delete(I id);

}
