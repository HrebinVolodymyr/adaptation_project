package copyright.epam.com.model;

import copyright.epam.com.entity.Role;

public class RoleModel {
	
	private Role role;
	
	public RoleModel(Role role) {
		this.role = role;
	}
	
	public Long getId() {
		return role.getId();
	}

	public String getName() {
		return role.getName();
	}

	public String getDescription() {
		return role.getDescription();
	}

}
