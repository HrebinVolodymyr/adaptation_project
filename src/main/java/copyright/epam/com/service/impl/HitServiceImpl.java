package copyright.epam.com.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import copyright.epam.com.entity.Hit;
import copyright.epam.com.repository.HitRepository;
import copyright.epam.com.service.HitService;

@Service
public class HitServiceImpl implements HitService{
	
	@Autowired
	private HitRepository hitRepository;

	@Override
	@Transactional
	public List<Hit> findAll() {	
		return Lists.newArrayList(hitRepository.findAll());
	}

	@Override
	public Hit findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void create(Hit entity) {
		hitRepository.save(entity);		
	}

	@Override
	public void update(Hit entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

}
