package copyright.epam.com.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import copyright.epam.com.dao.UserDAO;
import copyright.epam.com.entity.Group;
import copyright.epam.com.entity.Role;
import copyright.epam.com.entity.User;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
	
	@InjectMocks
	private UserServiceImpl userService = new UserServiceImpl();
	
	@Mock
	private UserDAO userDao;
	
	private Long EXIST_ROLE_ID = 1L;
	private Long NOT_EXIST_ROLE_ID = 4L;
	private List<User> users;
	
	@BeforeEach
	public void initRole() {
		users = new ArrayList<>();		
		
		Role roleAdmin = new Role(1L, "ADMIN", "admin");
		Role roleUser = new Role(2L, "USER", "user");
		Role roleAuthor = new Role(3L, "AUTHOR", "author");
				
		Group groupIntel = new Group(1L, "INTEL", "intel");
		Group groupAmd = new Group(2L, "AMD", "amd");
		Group groupIbm = new Group(3L, "IBM", "ibm");
		
		Set<Role> smithRoles = new HashSet<>();
		smithRoles.add(roleAdmin);		
		users.add(new User(1L, "OliverSmith","Oliver_Smith", "Oliver", "Smith", groupIntel, smithRoles));
		
		Set<Role> johnsonRoles = new HashSet<>();
		johnsonRoles.add(roleUser);
		users.add(new User(2L, "JackJohnson","Jack_Johnson", "Jack", "Johnson", groupAmd, johnsonRoles));
		
		Set<Role> williamsRoles = new HashSet<>();
		williamsRoles.add(roleAdmin);
		williamsRoles.add(roleAuthor);
		users.add(new User(3L, "HarryWilliams","Harry_Williams", "Harry", "Williams", groupIbm, williamsRoles));			
	}

	@Test
	public void getUsersByRoleIdTest() {	
		when(userDao.findAll()).thenReturn(users);
		
		Stream<User> usersStrem = users.stream();
		List<User> usersExpected  = usersStrem.filter(user -> {
			Set<Role> roles = user.getRoles();
			for (Role role : roles) {
				if(role.getId().equals(EXIST_ROLE_ID)) {
					return true;
				}
			}
			return false;
		}).sorted((firstUser, secondUser) -> firstUser.getFirstName().compareTo(secondUser.getFirstName()))
				.collect(Collectors.toList());		
		
		List<User> usersActual = userService.getUsersByRoleId(EXIST_ROLE_ID);
		
		assertEquals(usersExpected, usersActual);
	}
	
	@Test
	public void getUsersByRoleIdWhenRoleIdNotExist() {
		when(userDao.findAll()).thenReturn(users);
		
		List<User> usersExpected = new ArrayList<>();
		List<User> usersActual = userService.getUsersByRoleId(NOT_EXIST_ROLE_ID);
		
		assertEquals(usersExpected, usersActual);
	}
	
	@Test
	public void getUsersByRoleIdWhenIdIsNull() {
		assertThrows(IllegalArgumentException.class, () -> userService.getUsersByRoleId(null));
	}

}
