package copyright.epam.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableCaching
//@EnableElasticsearchRepositories(basePackages = "copyright.epam.com.repository")
public class Application {
	
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}