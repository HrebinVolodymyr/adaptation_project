package copyright.epam.com.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import copyright.epam.com.dao.RoleDAO;
import copyright.epam.com.entity.Role;
import copyright.epam.com.service.RoleService;

@Service
@CacheConfig(cacheNames = "roleCache")
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	RoleDAO roleDAO;

	@Override
	@Transactional
	public List<Role> findAll(){		
		return roleDAO.findAll();
	}

	@Override
	@Transactional
	@Cacheable
	public Role findById(Long id){
		return roleDAO.findById(id);
	}

	@Override
	@Transactional
	public void create(Role entity){
		 roleDAO.create(entity);	
	}	
	
	@Override
	@Transactional
	public void update(Role entity){
	    roleDAO.create(entity);		
	}	
	
	@Override
	@Transactional
	public void delete(Long id){
		roleDAO.delete(id);	
	}
}
