package copyright.epam.com.service;

import copyright.epam.com.entity.Role;

public interface RoleService extends GeneralService<Role, Long>{

}
