package copyright.epam.com.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import copyright.epam.com.entity.Hit;

public interface HitRepository extends ElasticsearchRepository<Hit, Long>{

}
