package copyright.epam.com.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import copyright.epam.com.dto.RoleDTO;
import java.util.Objects;

@Entity
@Table(name = "role")
public class Role {
		
	public Role() {
		super();		
	}	

	public Role(RoleDTO roleDTO) {
		super();
		this.name = roleDTO.getName();
		this.description = roleDTO.getDescription();
	}
	
	public Role(Long id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name", nullable = false, unique = true, length = 30)
	private String name;
	
	@Column(name = "description", nullable = true, length = 200)
	private String description;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
	private List<User> users;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, description);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Role) {
			Role that = (Role) object;
			return Objects.equals(this.id, that.id) && Objects.equals(this.name, that.name)
					&& Objects.equals(this.description, that.description);
		}
		return false;
	}	
	
}
