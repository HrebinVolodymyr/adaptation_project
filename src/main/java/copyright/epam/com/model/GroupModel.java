package copyright.epam.com.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import copyright.epam.com.entity.Group;
import copyright.epam.com.entity.User;

public class GroupModel {
	
	@JsonIgnore
	private Group group;
	
	public GroupModel(Group group) {
		this.group = group;
	}

	public Long getId() {
		return group.getId();
	}

	public String getName() {
		return group.getName();
	}	

	public String getDescription() {
		return group.getDescription();
	}
	
	public List<UserModel> getUsers() {
		List<UserModel> usersDTO = new ArrayList<>(group.getUsers().size());
		for (User user : group.getUsers()) {
			usersDTO.add(new UserModel(user));
		}
		return usersDTO;
	}

	
}
