package copyright.epam.com.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import copyright.epam.com.dto.UserDTO;
import java.util.Objects;

@Entity
@Table(name = "user")
public class User {	
		
	public User() {
		super();		
	}
	
	public User(UserDTO userDTO) {
		super();
		this.username = userDTO.getUsername();
		this.password = userDTO.getPassword();
		this.firstName = userDTO.getFirstName();
		this.lastName = userDTO.getLastName();
	}
	
	public User(Long id, String username, String password, String firstName, String lastName, Group userGroup,
			Set<Role> roles) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userGroup = userGroup;
		this.roles = roles;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "username", nullable = false, unique = true, length = 30)
	private String username;
	
	@Column(name = "password", nullable = true, length = 30)
	private String password;
	
	@Column(name = "firstName", nullable = false, length = 30)
	private String firstName;
	
	@Column(name = "lastName",  nullable = false, length = 30)
	private String lastName;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
	private Group userGroup;
	
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_role", 
        joinColumns = { @JoinColumn(name = "user_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "role_id") }
    )
	private Set<Role> roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Group getGroup() {
		return userGroup;
	}

	public void setGroup(Group userGroup) {
		this.userGroup = userGroup;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, username, password, firstName, lastName);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof User) {
			User that = (User) object;
			return Objects.equals(this.id, that.id) && Objects.equals(this.username, that.username)
					&& Objects.equals(this.password, that.password) && Objects.equals(this.firstName, that.firstName)
					&& Objects.equals(this.lastName, that.lastName);
		}
		return false;
	}	
}
