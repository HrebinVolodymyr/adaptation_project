package copyright.epam.com.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import copyright.epam.com.dto.UserDTO;
import copyright.epam.com.entity.Group;
import copyright.epam.com.entity.Role;
import copyright.epam.com.entity.User;
import copyright.epam.com.model.UserModel;
import copyright.epam.com.service.GroupService;
import copyright.epam.com.service.RoleService;
import copyright.epam.com.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	GroupService groupService;
	
	@Autowired
	RoleService roleService;
	
	@RequestMapping(value = "/api/user", method = RequestMethod.GET)
	public ResponseEntity<List<UserModel>> getUsers() {		
		List<User> users = userService.findAll();
		List<UserModel> usersModel = new ArrayList<>(users.size());		
		for (User user : users) {
			usersModel.add(new UserModel(user));
		}		
		return new ResponseEntity<>(usersModel, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/user/{userId}", method = RequestMethod.GET)
	public ResponseEntity<UserModel> getUserById(@PathVariable Long userId) {		
		User user = userService.findById(userId);		
		UserModel userModel = new UserModel(user);		
		return new ResponseEntity<>(userModel, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/api/user/{userId}", method = RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable Long userId) {
		userService.delete(userId);		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/user", method = RequestMethod.POST)
	public ResponseEntity<UserModel> addUser(@RequestBody UserDTO userDTO){		
		User user = new User(userDTO);
		Group userGroup = groupService.findById(userDTO.getGroupId());
		Set<Role> roles = new HashSet<>();
		for (Long roleId : userDTO.getRoleIds()) {
			roles.add(roleService.findById(roleId));
		}
		user.setGroup(userGroup);
		user.setRoles(roles);
		userService.create(user);
		UserModel userModel = new UserModel(user);
		return new ResponseEntity<>(userModel, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/api/user/{userId}", method = RequestMethod.PUT)
	public ResponseEntity<UserModel> updateGroup(@PathVariable Long userId, @RequestBody UserDTO updateUserDTO){	
		User user = userService.findById(userId);
		Group userGroup = groupService.findById(updateUserDTO.getGroupId());
		Set<Role> roles = new HashSet<>();
		for (Long roleId : updateUserDTO.getRoleIds()) {
			roles.add(roleService.findById(roleId));
		}
		user.setFirstName(updateUserDTO.getFirstName());
		user.setLastName(updateUserDTO.getLastName());
		user.setUsername(updateUserDTO.getUsername());
		user.setPassword(updateUserDTO.getPassword());
		user.setGroup(userGroup);
		user.setRoles(roles);
		userService.update(user);	
		UserModel userModel = new UserModel(user);
		return new ResponseEntity<>(userModel , HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/api/user/role/{roleId}", method = RequestMethod.GET)
	public ResponseEntity<List<UserModel>> getUsersByRoleId(@PathVariable Long roleId) {		
		List<User> users = userService.getUsersByRoleId(roleId);
		List<UserModel> usersModel = new ArrayList<>(users.size());		
		for (User user : users) {
			usersModel.add(new UserModel(user));
		}		
		return new ResponseEntity<>(usersModel, HttpStatus.OK);	
	}

}
