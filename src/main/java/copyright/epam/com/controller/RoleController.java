package copyright.epam.com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import copyright.epam.com.dto.RoleDTO;
import copyright.epam.com.entity.Role;
import copyright.epam.com.model.RoleModel;
import copyright.epam.com.service.RoleService;

@RestController
public class RoleController {
	
	@Autowired
	RoleService roleService;
	
	@RequestMapping(value = "/api/role", method = RequestMethod.GET)
	public ResponseEntity<List<RoleModel>> getRoles() {		
		List<Role> roles = roleService.findAll();	
		List<RoleModel> rolesModel = new ArrayList<>(roles.size());		
		for (Role role : roles) {
			rolesModel.add(new RoleModel(role));
		}					
		return new ResponseEntity<>(rolesModel, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/role/{roleId}", method = RequestMethod.GET)
	public ResponseEntity<RoleModel> getRoleById(@PathVariable Long roleId) {		
		Role role = roleService.findById(roleId);		
		RoleModel roleModel = new RoleModel(role);
		return new ResponseEntity<>(roleModel, HttpStatus.OK);				
	}
	
	@RequestMapping(value = "/api/role/{roleId}", method = RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteRole(@PathVariable Long roleId) {
		roleService.delete(roleId);		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/role", method = RequestMethod.POST)
	public ResponseEntity<RoleModel> addRole(@RequestBody RoleDTO newRoleDTO){	
		Role role = new Role(newRoleDTO);
		roleService.create(role);
		RoleModel roleModel = new RoleModel(role);
		return new ResponseEntity<>(roleModel, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/api/role/{roleId}", method = RequestMethod.PUT)
	public ResponseEntity<RoleModel> updateRole(@PathVariable Long roleId, @RequestBody RoleDTO updateRoleDTO){	
		Role role = roleService.findById(roleId);
		role.setName(updateRoleDTO.getName());
		role.setDescription(updateRoleDTO.getDescription());
		roleService.update(role);	
		RoleModel roleModel = new RoleModel(role);
		return new ResponseEntity<>(roleModel , HttpStatus.CREATED);
	}

}
