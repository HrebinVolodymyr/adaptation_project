package copyright.epam.com.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import copyright.epam.com.dao.GroupDAO;
import copyright.epam.com.entity.Group;
import copyright.epam.com.service.GroupService;

@Service
public class GroupServiceImpl implements GroupService{		
	
	@Autowired
	GroupDAO groupDAO;

	@Override
	@Transactional
	public List<Group> findAll(){		
		return groupDAO.findAll();
	}

	@Override
	@Transactional
	public Group findById(Long id){		
		return groupDAO.findById(id);
	}

	@Override
	@Transactional
	public void create(Group entity){
		 groupDAO.create(entity);		
	}

	@Override
	@Transactional
	public void update(Group entity){
	    groupDAO.update(entity);		
	}

	@Override
	@Transactional
	public void delete(Long id){
		groupDAO.delete(id);		
	}
	
}
