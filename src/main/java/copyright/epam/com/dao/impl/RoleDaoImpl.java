package copyright.epam.com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import copyright.epam.com.dao.RoleDAO;
import copyright.epam.com.entity.Role;

@Component
public class RoleDaoImpl  implements RoleDAO{
	
	@PersistenceContext
	EntityManager em;

	@Override
	public List<Role> findAll() {
		return em.createQuery("SELECT r FROM Role r", Role.class)
		        .getResultList();
	}

	@Override
	public Role findById(Long id) {
		return em.find(Role.class, id);
	}

	@Override
	public void create(Role entity) {
		em.persist(entity);		
	}

	@Override
	public void update(Role entity) {
		em.merge(entity);		
	}
	
	@Override
	public void delete(Long id) {
		em.remove(em.getReference(Role.class, id));
	}	
}
