package copyright.epam.com.entity;

import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

@Document(indexName = "copyright", type = "hit")
public class Hit {
	
	@Id
	private Long hitId;
	
	@Field(type = FieldType.Date)
	private Date feedingDate;
	
	private Long groupId;
	
	private Set<String> classesSearched;
	
	@Field(type = FieldType.Nested)
	private Set<Responsible> responsible;
	
	public Long getHitId() {
		return hitId;
	}
	
	public void setHitId(Long hitId) {
		this.hitId = hitId;
	}
	
	public Date getFeedingDate() {
		return feedingDate;
	}
	
	public void setFeedingDate(Date feedingDate) {
		this.feedingDate = feedingDate;
	}
	
	public Long getGroupId() {
		return groupId;
	}
	
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	public Set<String> getClassesSearched() {
		return classesSearched;
	}
	
	public void setClassesSearched(Set<String> classesSearched) {
		this.classesSearched = classesSearched;
	}
	
	public Set<Responsible> getResponsible() {
		return responsible;
	}
	
	public void setResponsible(Set<Responsible> responsible) {
		this.responsible = responsible;
	}

	@Override
	public int hashCode() {
		return Objects.hash(hitId, feedingDate, groupId, classesSearched, responsible);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Hit) {
			Hit that = (Hit) object;
			return Objects.equals(this.hitId, that.hitId) && Objects.equals(this.feedingDate, that.feedingDate)
					&& Objects.equals(this.groupId, that.groupId)
					&& Objects.equals(this.classesSearched, that.classesSearched)
					&& Objects.equals(this.responsible, that.responsible);
		}
		return false;
	}
}
