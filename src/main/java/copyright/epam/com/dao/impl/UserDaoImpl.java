package copyright.epam.com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import copyright.epam.com.dao.UserDAO;
import copyright.epam.com.entity.User;

@Component
public class UserDaoImpl implements UserDAO {
	
	@PersistenceContext
	EntityManager em;

	@Override
	public List<User> findAll() {		
		return em.createQuery("SELECT u FROM User u", User.class)
		        .getResultList();
	}

	@Override
	public User findById(Long id) {		
		return em.find(User.class, id);
	}

	@Override
	public void create(User entity) {
		em.persist(entity);
		
	}

	@Override
	public void update(User entity) {
		em.merge(entity);		
	}

	@Override
	public void delete(Long id) {
		em.remove(em.getReference(User.class, id));
	}
}
