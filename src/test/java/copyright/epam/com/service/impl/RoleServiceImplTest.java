package copyright.epam.com.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import copyright.epam.com.dao.RoleDAO;
import copyright.epam.com.entity.Role;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTest {
	
	@InjectMocks
	RoleServiceImpl roleService = new RoleServiceImpl();
	
	@Mock
	RoleDAO roleDao;
	
	private Role role;		
	
	@BeforeEach
	public void initRole() {				
		role = new Role(1L, "TESTER", "tester");				
	}
	
	@AfterEach
	public void cleanRole() {
		role = null;		
	}
		
	@Test
	public void createTest() {
		doAnswer((Answer<?>) invocation -> {
			Object role = invocation.getArgument(0);			
			assertEquals(this.role, role);
			return null;
		}).when(roleDao).create(any(Role.class));
		
		roleService.create(role);
	}
		
	@Test
	public void findByIdTest() {
		Role ex = new Role();
		ex.setId(1L);
		ex.setName("TESTER");
		ex.setDescription("tester");
		
		when(roleDao.findById(1L)).thenReturn(this.role);		
		assertEquals(ex, roleService.findById(1L));
	}	
	
}
