package copyright.epam.com.service;

import java.util.List;

public interface GeneralService<T, I> {

  List<T> findAll();

  T findById(I id);

  void create(T entity);

  void update(T entity); 

  void delete(I id);
}
