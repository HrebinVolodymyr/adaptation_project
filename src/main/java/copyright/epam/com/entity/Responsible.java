package copyright.epam.com.entity;

import java.util.Objects;

public class Responsible {
	
	private Long userId;
	private String responsibleType;
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getResponsibleType() {
		return responsibleType;
	}
	
	public void setResponsibleType(String responsibleType) {
		this.responsibleType = responsibleType;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(userId, responsibleType);
	}
	@Override
	public boolean equals(Object object) {
		if (object instanceof Responsible) {
			Responsible that = (Responsible) object;
			return Objects.equals(this.userId, that.userId)
					&& Objects.equals(this.responsibleType, that.responsibleType);
		}
		return false;
	}
}
