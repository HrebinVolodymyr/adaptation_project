package copyright.epam.com.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import copyright.epam.com.entity.Role;
import copyright.epam.com.entity.User;

public class UserModel {
	
	@JsonIgnore
	private User user;

	public UserModel(User user) {
		this.user = user;
	}	
	
	public Long getId() {
		return user.getId();
	}

	public String getUsername() {
		return user.getUsername();
	}
	
	public String getFirstName() {
		return user.getFirstName();
	}

	public String getLastName() {
		return user.getLastName();
	}

	public Long getGroupId() {
		return user.getGroup().getId();
	}
	
	public List<RoleModel> getRoles() {
		List<RoleModel> roleDTO = new ArrayList<>(user.getRoles().size());
		for (Role role : user.getRoles()) {
			roleDTO.add(new RoleModel(role));
		}
		return roleDTO;
	}

}
