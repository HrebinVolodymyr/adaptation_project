package copyright.epam.com.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import copyright.epam.com.dao.UserDAO;
import copyright.epam.com.entity.Role;
import copyright.epam.com.entity.User;
import copyright.epam.com.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserDAO userDAO;

	@Override
	@Transactional
	public List<User> findAll(){		
		return userDAO.findAll();
	}

	@Override
	@Transactional
	public User findById(Long id){
		return userDAO.findById(id);
	}

	@Override
	@Transactional
	public void create(User entity){		
		userDAO.create(entity);		   
	}

	@Override
	@Transactional
	public void update(User entity){
		userDAO.update(entity);		
	}

	@Override
	@Transactional
	public void delete(Long id){
		userDAO.delete(id);	
	}

	@Override
	public List<User> getUsersByRoleId(Long roleId) {
		if(roleId == null) {
			throw new IllegalArgumentException();
		}
		List<User> users = userDAO.findAll();
		Stream<User> usersStrem = users.stream();
		List<User> usersResult = null;
		usersResult = usersStrem.filter(user -> {
			Set<Role> roles = user.getRoles();
			for (Role role : roles) {
				if(role.getId().equals(roleId)) {
					return true;
				}
			}
			return false;
		}).sorted((firstUser, secondUser) -> firstUser.getFirstName().compareTo(secondUser.getFirstName()))
				.collect(Collectors.toList());		
		return usersResult;
	}
	
}
