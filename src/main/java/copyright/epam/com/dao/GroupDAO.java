package copyright.epam.com.dao;

import copyright.epam.com.entity.Group;

public interface GroupDAO extends GeneralDAO<Group, Long>{

}
