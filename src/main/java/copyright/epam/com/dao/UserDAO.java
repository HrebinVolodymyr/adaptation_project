package copyright.epam.com.dao;

import copyright.epam.com.entity.User;

public interface UserDAO extends GeneralDAO<User, Long>{

}
